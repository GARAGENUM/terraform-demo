## TERRAFORM
terraform {
  required_version = ">= 0.12"
}

## PROVIDER
provider "google" {
  project     = var.gcp_project_id
  credentials = file(var.gcp_auth_file)
  region      = var.gcp_region
}

## NETWORK
resource "google_compute_network" "vpc_network" {
  name                    = "my-network"
  auto_create_subnetworks = false
  mtu                     = 1460
}

resource "google_compute_subnetwork" "default" {
  name          = "my-subnet"
  ip_cidr_range = "10.0.1.0/24"
  region        = var.gcp_region
  network       = google_compute_network.vpc_network.id
}

## FIREWALL
### SSH
resource "google_compute_firewall" "ssh" {
  name = "allow-ssh"
  allow {
    ports    = ["22"]
    protocol = "tcp"
  }
  direction     = "INGRESS"
  network       = google_compute_network.vpc_network.id
  priority      = 1000
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ssh"]
}

### APP
resource "google_compute_firewall" "flask" {
  name    = "flask-app-firewall"
  network = google_compute_network.vpc_network.id

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  source_ranges = ["0.0.0.0/0"]
}


## VM
resource "google_compute_instance" "default" {
  name         = "flask-vm"
  machine_type = "e2-micro"
  zone         = var.gcp_zone
  tags         = ["ssh"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  metadata = {
    ssh-keys = "${var.gcp_ssh_user}:${file(var.gcp_ssh_pub_key_file)}"
  }

  network_interface {
    subnetwork = google_compute_subnetwork.default.id

    access_config {
      #### TO HAVE A PUBLIC IP ADRESS
    }
  }

  ### COPY DES FICHIERS
  provisioner "file" {
    source      = "app/"
    destination = "./"
    connection {
      host        = self.network_interface.0.access_config.0.nat_ip
      type        = "ssh"
      user        = var.gcp_ssh_user
      timeout     = "500s"
      private_key = file(var.gcp_ssh_priv_key_file)
    }
  }

  ### START DEPLOY.SH
  provisioner "remote-exec" {
    inline = [
      "chmod +x ~/deploy.sh",
      "cd ~",
      "./deploy.sh"
    ]
    connection {
      host        = self.network_interface.0.access_config.0.nat_ip
      type        = "ssh"
      user        = var.gcp_ssh_user
      timeout     = "500s"
      private_key = file(var.gcp_ssh_priv_key_file)
    }
  }
}

## GET VM PUBLIC IP
output "Web-server-URL" {
  value = join("", ["http://", google_compute_instance.default.network_interface.0.access_config.0.nat_ip,])
}