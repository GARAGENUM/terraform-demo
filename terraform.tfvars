# GCP Settings
gcp_project_id        = "terraform-demo-381114"                          # A MODIFIER
gcp_region            = "europe-west4"
gcp_zone              = "europe-west4-a"
gcp_auth_file         = "./auth/cle.json" # A MODIFIER
gcp_ssh_user          = "devops"                                         # A MODIFIER OU PAS
gcp_ssh_pub_key_file  = "~/.ssh/terraform.pub"
gcp_ssh_priv_key_file = "~/.ssh/terraform"