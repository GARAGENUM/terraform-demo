# GCP authentication file
variable "gcp_auth_file" {
  type        = string
  description = "GCP authentication file"
}

# define GCP region
variable "gcp_region" {
  type        = string
  description = "GCP region"
}

# define GCP project name
variable "gcp_project_id" {
  type        = string
  description = "GCP project id"
}

# define GCP zone
variable "gcp_zone" {
  type        = string
  description = "GCP zone"
}

variable "gcp_ssh_user" {
  type        = string
  description = "SSH user"
}

variable "gcp_ssh_pub_key_file" {
  type        = string
  description = "SSH public key file path"
}

variable "gcp_ssh_priv_key_file" {
  type        = string
  description = "SSH private key file path"
}