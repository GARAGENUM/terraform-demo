# TERRAFORM / GCP

Demo Terraform déployant une application Flask dans un conteneur Docker sur GCP

## PRE REQUIS

- [COMPTE GCP](https://cloud.google.com/?hl=fr)
- [TERRAFORM](https://www.terraform.io/)

## PREPARATION

### MACHINE LOCALE

- Installer Terraform:
```bash
# LINUX INSTALL
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
```

- Créer une paire de clé ssh dédiée pour Terraform dans ~/.ssh
```bash
# NOMMER LE CLE TERRAFORM
ssh-keygen -t rsa -b 4096
```

### DASHBOARD GCP

- Créer un projet "terraform-demo"
- Séletionner le projet créé puis créer un compte de service dans l'onglet [IAM et administration](https://console.cloud.google.com/projectselector2/iam-admin/serviceaccounts?hl=fr) de la console GCP
- Ajouter les rôles suivant:
    - Compute admin
    - Compute network admin
    - Service account admin
- Activer les APIs nécessaires (compute engine API, storage API, cloud billing API)

![SERVICES ACCOUNT](docs/service-account.png)

- Créer une clé au sein du compte de service avec les droits sur le compute engine (pour créer les VMs), et télécharger le fichier json contenant la clé pour le mettre dans le dossier auth/

### TERRAFORM.TFVARS

- Modifier le fichier terraform.tfvars en fonction de votre environnement (gcp_project_id et gcp_auth_file)

## UTILISATION

- Initialiser Terraform:
```bash
cd terraform-demo
terraform init
```

- Dry run:
```bash
terraform plan
```

- Lançer Terraform:
```bash
terraform apply
# Saisir yes quand demandé
```

> Visiter l'adresse fournie en output du terraform apply (Web-server-URL = "http://SERVER_IP:5000")

- Stopper Terraform:
```bash
terraform destroy
```