#!/bin/bash

sudo apt-get update
sudo apt-get install -yq docker.io
sudo docker build --network=host -t flaskapp .
sudo docker run -d -p 5000:5000 flaskapp:latest